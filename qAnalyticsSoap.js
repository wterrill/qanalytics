const soapRequest = require('easy-soap-request');
const fs = require('fs');
var DOMParser = require('dom-parser');
var format = require('xml-formatter');
const jsdom = require("jsdom");


const { JSDOM } = jsdom;
parser = new DOMParser();

// example data
const url = 'http://ww2.qanalytics.cl/ws_tottus_estado_entrega/service.asmx';
 const headers = {
  'Content-Type': 'text/xml;charset=UTF-8',
 }
//   'soapAction': 'http://tempuri.org/WM_INS_ESTADO_ENTREGA',
// };
const xml = fs.readFileSync('./SOAP.xml', 'utf-8');

// usage of module
(async () => {
  const { response } = await soapRequest(url, headers, xml, 1000); // Optional timeout parameter(milliseconds)
  const { body, statusCode } = response;
  console.log(body);
  console.log(statusCode);

//   beer = parser.DOMParser
  xmlDoc = parser.parseFromString(body,"text/xml");
  var formattedXml = format(body);
  console.log(formattedXml);
  const dom = new JSDOM(body);
  console.log("#######")
  console.log(dom.window.document.querySelector("WM_INS_ESTADO_ENTREGAResult").textContent);
})
().catch(err => {
    console.log(err)
});
