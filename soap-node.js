const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const DOMParser = require('xmldom').DOMParser;

function parseXml(text) {
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(text, "text/xml");
    Array.from(xmlDoc.getElementsByTagName("reference")).forEach(function (item) {
        console.log('Title: ', item.childNodes[3].childNodes[0].nodeValue);
    });

}

function soapRequest(url, payload) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', url, true);

    // build SOAP request
    xmlhttp.onreadystatechange = (response => {
        console.log("entered here")
        console.log(xmlhttp.status)
        console.log(xmlhttp)
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                parseXml(xmlhttp.responseText);
            }
        }
    })

    // Send the POST request

    xmlhttp.setRequestHeader('Content-Type', 'text/xml');
    xmlhttp.setRequestHeader('SOAPAction', "http://tempuri.org/WM_INS_ESTADO_ENTREGA");
    xmlhttp.setRequestHeader('Content-Length', "");
    xmlhttp.setRequestHeader('Host', "ww2.qanalytics.cl");

    xmlhttp.send(payload);
}

// soapRequest('https://www.ebi.ac.uk/europepmc/webservices/soap', 
//     `<?xml version="1.0" encoding="UTF-8"?>
//     <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
//     <S:Header />
//     <S:Body>
//         <ns4:getReferences xmlns:ns4="http://webservice.cdb.ebi.ac.uk/"
//             xmlns:ns2="http://www.scholix.org"
//             xmlns:ns3="https://www.europepmc.org/data">
//             <id>C7886</id>
//             <source>CTX</source>
//             <offSet>0</offSet>
//             <pageSize>25</pageSize>
//             <email>ukpmc-phase3-wp2b---do-not-reply@europepmc.org</email>
//         </ns4:getReferences>
//     </S:Body>
//     </S:Envelope>`);

//http://tempuri.org/WM_INS_ESTADO_ENTREGA

soapRequest('http://ww2.qanalytics.cl/ws_tottus_estado_entrega/service.asmx', 
`<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
            <Authentication xmlns="http://tempuri.org/">
                <Usuario>WS_Tottus</Usuario>
                <Clave>$$Tot19#</Clave>
            </Authentication>
        </soap:Header>
        <soap:Body>
            <WM_INS_ESTADO_ENTREGA xmlns="http://tempuri.org/">
                <XML_DETALLE>
                    <DATOS>
                        <VIAJE>111111</VIAJE>
                        <CONDUCTOR_CODIGO>222222227</CONDUCTOR_CODIGO>
                        <CONDUCTOR_NOMBRE>Will Terrill</CONDUCTOR_NOMBRE>
                        <PATENTE>ABC123</PATENTE>
                        <PEDIDO>555777</PEDIDO>
                        <MATERIAL_CODIGO>777777</MATERIAL_CODIGO>
                        <MATERIAL_DETALLE>Lomo Vetado</MATERIAL_DETALLE>
                        <MATERIAL_CODIGO>88888</MATERIAL_CODIGO>
                        <MATERIAL_DETALLE>Lomo Vetado otro</MATERIAL_DETALLE>
                        <MATERIAL_CODIGO>99999</MATERIAL_CODIGO>
                        <MATERIAL_DETALLE>Lomo Vetado aún otro</MATERIAL_DETALLE>
                        <FECHA_ENTREGA>20190918 16:00:00</FECHA_ENTREGA>
                        <ENTREGA_CODIGO>05</ENTREGA_CODIGO>
                        <ENTREGA_DETALLE>Recepción Con Observación</ENTREGA_DETALLE>
                        <ENTREGA_RECHAZO_CODIGO>01</ENTREGA_RECHAZO_CODIGO>
                        <ENTREGA_RECHAZO_DETALLE>Sin Moradores</ENTREGA_RECHAZO_DETALLE>
                        <INTEGRACION_ORIGEN>Chilexpress-TEST</INTEGRACION_ORIGEN>
                    </DATOS>
                </XML_DETALLE>
            </WM_INS_ESTADO_ENTREGA>
        </soap:Body>
    </soap:Envelope>`);

    